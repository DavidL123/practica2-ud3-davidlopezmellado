/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.hibernate.gui;

import com.davidlm.hibernate.base.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.ArrayList;


/**
 * Clase Método que contiene los métodos que tienen que ver con la gestión de la base de datos.
 */
public class Modelo {
    SessionFactory sessionFactory;

    /**
     * Método que nos permitirá desconectar de la base de datos.
     */
    public void desconectar() {
        //Cierro la factoria de sessiones
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
    }

    /**
     * Método que nos permitirá conectar con la base de datos.
     */
    public void conectar() {
        Configuration configuracion = new Configuration();

        configuracion.configure("hibernate.cfg.xml");

        configuracion.addAnnotatedClass(Cliente.class);
        configuracion.addAnnotatedClass(Pedido.class);
        configuracion.addAnnotatedClass(Habitacion.class);
        configuracion.addAnnotatedClass(Agencia.class);
        configuracion.addAnnotatedClass(ClienteHabitacion.class);
        configuracion.addAnnotatedClass(HabitacionAgencia.class);

        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();


        sessionFactory = configuracion.buildSessionFactory(ssr);

    }


    /**
     * Método que nos permitirá dar de alta un registro en la base de datos en la tabla Agencia.
     * @param nuevoAgencia Objeto de la clase Agencia
     */
    //MÉTODOS AGENCIA
    public void altaAgencia(Agencia nuevoAgencia) {

        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoAgencia);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Método que recibe un objeto de la clase Agencia y nos permitirá modificarlo con los datos que hemos recibido.
     * @param agenciaSeleccion Objeto de la clase Agencia.
     */
    public void modificarAgencia(Agencia agenciaSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(agenciaSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que nos permitirá borrar un dato de la base de datos, recibiendo un objeto de la clase Agencia.
     * @param agenciaSeleccion Objeto de la clase Agencia.
     */
    public void borrarAgencia(Agencia agenciaSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(agenciaSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que nos devuelve un arraylist con los datos de la tabla agencias.
     * @return ArrayList<Agencia>
     */
    public ArrayList<Agencia> getAgencias() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Agencia ");
        ArrayList<Agencia> listaAgencias = (ArrayList<Agencia>)query.getResultList();
        sesion.close();
        return listaAgencias;
    }


    /**
     * Método que nos crea un registro en la base de datos, recibiendo un objeto de la clase Cliente.
     * @param nuevoCliente Objeto de la clase Cliente
     */

    //MÉTODOS CLIENTE
    public void altaCliente(Cliente nuevoCliente) {

        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoCliente);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Método que nos devuelve un arraylist con los datos de la tabla clientes.
     * @return ArrayList<Cliente>
     */
    public ArrayList<Cliente> getClientes() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Cliente ");
        ArrayList<Cliente> lista = (ArrayList<Cliente>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Método que nos permitirá modificar un registro de la tabla Cliente, recibiendo un objeto de la clase Cliente.
     * @param clienteSeleccion Objeto de la clase Cliente
     */
    public void modificarCliente(Cliente clienteSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(clienteSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que nos permitirá borrar un cliente de la tabla Clientes, recibiendo un objeto de la clase Cliente.
     * @param clienteSeleccion Objeto de la clase Cliente
     */
    public void borrarCliente(Cliente clienteSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(clienteSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }


    /**
     * Método que nos permite crear un registro de la tabla habitaciones. Recibe un objeto de la clase Habitacion.
     * @param nuevaHabitacion Objeto de la clase Habitacion
     */

    //MÉTODOS HABITACION
    public void altaHabitacion(Habitacion nuevaHabitacion) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevaHabitacion);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Método que nos devuelve un arraylist con los datos de la tabla habitaciones.
     * @return ArrayList<Habitacion>
     */
    public ArrayList<Habitacion> getHabitaciones() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Habitacion ");
        ArrayList<Habitacion> lista = (ArrayList<Habitacion>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Método que nos permite modificar un registro de la tabla habitacion. Recibe un objeto de la clase Habitacion.
     * @param habitacionSeleccion Objeto de la clase Habitacion
     */
    public void modificarHabitacion(Habitacion habitacionSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(habitacionSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que nos permite borrar un registro de la tabla habitacion. Recibe un objeto de la clase Habitacion.
     * @param habitacionSeleccion Objeto de la clase Habitacion
     */
    public void borrarHabitacion(Habitacion habitacionSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(habitacionSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }


    /**
     * Método que te crea un registro con datos en la tabla pedidos. Recibe un objeto de la clase Pedido.
     * @param nuevoPedido Objeto de la clase Pedido
     */
    //MÉTODOS PEDIDO
    public void altaPedido(Pedido nuevoPedido) {

        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoPedido);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Método que nos devuelve un arraylist con los datos de la tabla pedidos.
     * @return ArrayList<Pedido>
     */
    public ArrayList<Pedido> getPedidos() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Pedido ");
        ArrayList<Pedido> lista = (ArrayList<Pedido>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Método que nos permite modificar un registro de la tabla pedidos. Recibe un objeto de la clase Pedido.
     * @param pedidoSeleccion Objeto de la clase Pedido
     */
    public void modificarPedido(Pedido pedidoSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(pedidoSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que nos permite borrar un pedido de la tabla pedidos. Recibe un objeto de la clase Pedido.
     * @param pedidoSeleccion Objeto de la clase Pedido
     */
    public void borrarPedido(Pedido pedidoSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(pedidoSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }


    /**
     * Método que nos permite crear un registro de la tabla habitacion_agencia.
     * @param nuevoHabitacionAgencia ArrayList<HabitacionAgencia>
     */
    //MÉTODOS Habitación agencia
    public void altaHabitacionAgencia(HabitacionAgencia nuevoHabitacionAgencia) {

        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoHabitacionAgencia);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Método que nos devuelve un arraylist con los datos de la tabla habitacion_agencia.
     * @return ArrayList<HabitacionAgencia>
     */
    public ArrayList<HabitacionAgencia> getHabitacionAgencias() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM HabitacionAgencia ");
        ArrayList<HabitacionAgencia> lista = (ArrayList<HabitacionAgencia>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Método que nos permite modificar un registro de la tabla habitacion_agencia. Reciben un objeto de la clase HabitacionAgencia.
     * @param habitacionAgenciaSeleccion Objeto de la clase HabitacionAgencia
     */
    public void modificarHabitacionAgencia(HabitacionAgencia habitacionAgenciaSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(habitacionAgenciaSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que nos permite borrar un registro de la tabla habitacion_agencia. Recibe un objeto de la clase HabitacionAgencia.
     * @param habitacionAgenciaSeleccion Objeto de la clase HabitacionAgencia
     */
    public void borrarHabitacionAgencia(HabitacionAgencia habitacionAgenciaSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(habitacionAgenciaSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }


    /**
     * Método que nos crea un registro con datos en la tabla cliente_habitacion. Recibe un objeto de la clase ClienteHabitacion
     * @param nuevoClienteHabitacion Objeto de la clase ClienteHabitacion
     */
    //MÉTODOS Cliente habitación
    public void altaClienteHabitacion(ClienteHabitacion nuevoClienteHabitacion) {

        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoClienteHabitacion);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Método que nos devuelve un arraylist con los datos de la tabla cliente_habitacion.
     * @return ArrayList<ClienteHabitacion>
     */
    public ArrayList<ClienteHabitacion> getClienteHabitaciones() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM ClienteHabitacion ");
        ArrayList<ClienteHabitacion> lista = (ArrayList<ClienteHabitacion>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Método que nos permite modificar un registro de la tabla cliente_habitacion. Recibe un objeto de la clase ClienteHabitacion
     * @param clienteHabitacionSeleccion Objeto de la clase ClienteHabitacion
     */
    public void modificarClienteHabitacion(ClienteHabitacion clienteHabitacionSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(clienteHabitacionSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Método que nos permite borrar un registro de la tabla cliente_habitacion. Reciben un objeto de la clase ClienteHabitacion.
     * @param clienteHabitacionSeleccion Objeto de la clase ClienteHabitacion
     */
    public void borrarClienteHabitacion(ClienteHabitacion clienteHabitacionSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(clienteHabitacionSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }


}
