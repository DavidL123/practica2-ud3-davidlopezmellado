/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.hibernate.gui;


import com.davidlm.hibernate.base.*;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class Controlador  implements ActionListener, ListSelectionListener, ChangeListener, FocusListener, MouseListener, ItemListener{
    private Vista vista;
    private Modelo modelo;

    /**
     * Constructor de la clase Controlador, recibe los objetos Vista y Modelo. También añade y configura los métodos
     * addActionListeners, addListSelectionListener, addFocusListeners y  addItemListeners.
     * @param vista
     * @param modelo
     */
    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        addActionListeners(this);
        addListSelectionListener(this);
        addFocusListeners(this);
        addItemListeners(this);
    }


    /**
     * Método que recibirá la señal de cuando usemos un botón. Según el action command que tenga, a través del switch, ejecutará
     * una serie de órdenes. También después de realizar las accciones configuradas, ejecutará los métodos que nos visualizará
     * los datos que tengamos en la base de datos en cada JList correspondiente.
     * @param e ActionEvent Recibe el dato emitido, normalmente al pulsar un botón.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch(comando){
            case "Salir":
                modelo.desconectar();
                System.exit(0);
                break;
            case "Conectar":
                vista.conexionItem.setEnabled(false);
                modelo.conectar();
                break;

            case "Alta Agencia":

                Agencia nuevaAgencia = new  Agencia();
                nuevaAgencia.setNombre(vista.txtNombreAgencia.getText());
                nuevaAgencia.setCodigoEmpresa(vista.txtCodigoEmpresa.getText());
                nuevaAgencia.setFechaCreacion(Date.valueOf(vista.dateFechaCreacionAgencia.getDate()));
                nuevaAgencia.setTelefono(Integer.parseInt(vista.txtTelefonoAgencia.getText()));
                nuevaAgencia.setDireccion(vista.txtDireccionAgencia.getText());
                nuevaAgencia.setCorreo(vista.txtCorreoElectronicoAgencia.getText());
                modelo.altaAgencia(nuevaAgencia);

                break;

            case "Listar Agencias":
                listarAgencias(modelo.getAgencias());
                break;

            case "Modificar Agencia":
                Agencia agenciaSeleccion = (Agencia)vista.listAgencia.getSelectedValue();
                agenciaSeleccion.setNombre(vista.txtNombreAgencia.getText());
                agenciaSeleccion.setCodigoEmpresa(vista.txtCodigoEmpresa.getText());
                agenciaSeleccion.setFechaCreacion(Date.valueOf(vista.dateFechaCreacionAgencia.getDate()));
                agenciaSeleccion.setTelefono(Integer.parseInt(vista.txtTelefonoAgencia.getText()));
                agenciaSeleccion.setDireccion(vista.txtDireccionAgencia.getText());
                agenciaSeleccion.setCorreo(vista.txtCorreoElectronicoAgencia.getText());
                //agenciaSeleccion.setPropietario((Propietario)vista.listPropietarios.getSelectedValue());
                //System.out.println((Propietario)vista.listPropietarios.getSelectedValue());
                modelo.modificarAgencia(agenciaSeleccion);
                break;

            case "Borrar Agencia":
                Agencia agenciaBorrada  = (Agencia)vista.listAgencia.getSelectedValue();
                modelo.borrarAgencia(agenciaBorrada);
                break;


            case "Alta Cliente":

                Cliente nuevoCliente = new  Cliente();
                nuevoCliente.setNombre(vista.txtNombreCliente.getText());
                nuevoCliente.setApellidos(vista.txtApellidosCliente.getText());
                nuevoCliente.setFechaNacimiento(Date.valueOf(vista.datePickerFechaNCliente.getDate()));
                nuevoCliente.setDomicilio(vista.txtDomicilioCliente.getText());
                nuevoCliente.setDni(vista.txtDniCliente.getText());
                nuevoCliente.setNumeroMovil(Integer.parseInt(vista.txtMovilCliente.getText()));
                nuevoCliente.setTipoCliente(vista.txtTipoCliente.getText());
                modelo.altaCliente(nuevoCliente);

                break;

            case "Listar Clientes":
                listarClientes(modelo.getClientes());
                break;

            case "Modificacion Cliente":
                Cliente clienteSeleccion = (Cliente)vista.listCliente.getSelectedValue();
                clienteSeleccion.setNombre(vista.txtNombreCliente.getText());
                clienteSeleccion.setApellidos(vista.txtApellidosCliente.getText());
                clienteSeleccion.setFechaNacimiento(Date.valueOf(vista.datePickerFechaNCliente.getDate()));
                clienteSeleccion.setDomicilio(vista.txtDomicilioCliente.getText());
                clienteSeleccion.setDni(vista.txtDniCliente.getText());
                clienteSeleccion.setNumeroMovil(Integer.parseInt(vista.txtMovilCliente.getText()));
                clienteSeleccion.setTipoCliente(vista.txtTipoCliente.getText());
                //agenciaSeleccion.setPropietario((Propietario)vista.listPropietarios.getSelectedValue());
                //System.out.println((Propietario)vista.listPropietarios.getSelectedValue());
                modelo.modificarCliente(clienteSeleccion);
                break;

            case "Eliminar cliente":
                Cliente clienteBorrado  = (Cliente)vista.listCliente.getSelectedValue();
                modelo.borrarCliente(clienteBorrado);
                break;

            case "Alta Habitacion":

                Habitacion nuevaHabitacion = new  Habitacion();
                nuevaHabitacion.setNumeroHabitacion(Integer.parseInt(vista.txtNumeroHabitacion.getText()));
                nuevaHabitacion.setTipo(vista.txtTipoHabitacion.getText());
                nuevaHabitacion.setPrecioNoche(Double.parseDouble(vista.txtPrecioNocheHabitacion.getText()));
                nuevaHabitacion.setVistas(vista.txtVistasHabitacion.getText());
                nuevaHabitacion.setExtras(vista.txtExtrasHabitacion.getText());

                modelo.altaHabitacion(nuevaHabitacion);

                break;

            case "Listar Habitaciones":
                listarHabitaciones(modelo.getHabitaciones());
                break;

            case "Modificar Habitacion":
                Habitacion habitacionSeleccion = (Habitacion)vista.listHabitacion.getSelectedValue();
                habitacionSeleccion.setNumeroHabitacion(Integer.parseInt(vista.txtNumeroHabitacion.getText()));
                habitacionSeleccion.setTipo(vista.txtTipoHabitacion.getText());
                habitacionSeleccion.setPrecioNoche(Double.parseDouble(vista.txtPrecioNocheHabitacion.getText()));
                habitacionSeleccion.setVistas(vista.txtVistasHabitacion.getText());
                habitacionSeleccion.setExtras(vista.txtExtrasHabitacion.getText());
                //agenciaSeleccion.setPropietario((Propietario)vista.listPropietarios.getSelectedValue());
                //System.out.println((Propietario)vista.listPropietarios.getSelectedValue());
                modelo.modificarHabitacion(habitacionSeleccion);
                break;

            case "Eliminar Habitacion":
                Habitacion habitacionBorrado  = (Habitacion)vista.listHabitacion.getSelectedValue();
                modelo.borrarHabitacion(habitacionBorrado);
                break;
            case "AltaPedido":

                Pedido nuevoPedido = new Pedido();
                nuevoPedido.setPlatoComida(vista.txtPlatoComidaPedido.getText());
                nuevoPedido.setBebida(vista.txtBebidaPedido.getText());
                nuevoPedido.setFechaConsumicion(Date.valueOf(vista.datePickerPedido.getDate()));
                nuevoPedido.setPrecioTotal(Double.parseDouble(vista.txtPrecioTotal.getText()));
                nuevoPedido.setCliente((Cliente) vista.dcbmClientePedido.getSelectedItem());
                modelo.altaPedido(nuevoPedido);

                break;

            case "Listar pedidos":
                listarPedidos(modelo.getPedidos());
                break;

            case "Modificar Pedido":
                Pedido pedidoSeleccion = (Pedido)vista.listPedido.getSelectedValue();
                pedidoSeleccion.setPlatoComida(vista.txtPlatoComidaPedido.getText());
                pedidoSeleccion.setBebida(vista.txtBebidaPedido.getText());
                pedidoSeleccion.setFechaConsumicion(Date.valueOf(vista.datePickerPedido.getDate()));
                pedidoSeleccion.setPrecioTotal(Double.parseDouble(vista.txtPrecioTotal.getText()));
                pedidoSeleccion.setCliente((Cliente)vista.dcbmClientePedido.getSelectedItem());
                //agenciaSeleccion.setPropietario((Propietario)vista.listPropietarios.getSelectedValue());
                //System.out.println((Propietario)vista.listPropietarios.getSelectedValue());
                modelo.modificarPedido(pedidoSeleccion);
                break;

            case "Eliminar Pedido":
                Pedido pedidoBorrado  = (Pedido)vista.listPedido.getSelectedValue();
                modelo.borrarPedido(pedidoBorrado);
                break;

            case "Listar clientesPedido":
                listarClientesEnPedidos(modelo.getClientes());
                break;

            case "AltaHA":

                HabitacionAgencia nuevoHA = new HabitacionAgencia();
                nuevoHA.setFechaInicioReserva(Date.valueOf(vista.datePickerFechaIniHA.getDate()));
                nuevoHA.setFechaFinReserva(Date.valueOf(vista.datePickerFechaFinHA.getDate()));
                nuevoHA.setPrecioTotal(Double.parseDouble(vista.txtPrecioTotalHA.getText()));
                nuevoHA.setAgencia((Agencia) vista.dcbmAgenciaAH.getSelectedItem());
                nuevoHA.setHabitacion((Habitacion) vista.dcbmHabitacionAH.getSelectedItem());
                modelo.altaHabitacionAgencia(nuevoHA);

                break;

            case "ListarHA":
                listarHabitacionAgencias(modelo.getHabitacionAgencias());
                System.out.println("listaha");
                break;

            case "ModificarHA":
                HabitacionAgencia habitacionAgenciaSeleccion = (HabitacionAgencia)vista.listHabitacionAgencia.getSelectedValue();
                habitacionAgenciaSeleccion.setFechaInicioReserva(Date.valueOf(vista.datePickerFechaIniHA.getDate()));
                habitacionAgenciaSeleccion.setFechaFinReserva(Date.valueOf(vista.datePickerFechaFinHA.getDate()));
                habitacionAgenciaSeleccion.setPrecioTotal(Double.parseDouble(vista.txtPrecioTotalHA.getText()));
                habitacionAgenciaSeleccion.setAgencia((Agencia) vista.dcbmAgenciaAH.getSelectedItem());
                habitacionAgenciaSeleccion.setHabitacion((Habitacion) vista.dcbmHabitacionAH.getSelectedItem());

                //agenciaSeleccion.setPropietario((Propietario)vista.listPropietarios.getSelectedValue());
                //System.out.println((Propietario)vista.listPropietarios.getSelectedValue());
                modelo.modificarHabitacionAgencia(habitacionAgenciaSeleccion);
                break;

            case "EliminarHA":
                HabitacionAgencia habitacionAgenciaBorrado  = (HabitacionAgencia)vista.listHabitacionAgencia.getSelectedValue();
                modelo.borrarHabitacionAgencia(habitacionAgenciaBorrado);
                break;
            case "AltaCH":

                ClienteHabitacion nuevoCH = new ClienteHabitacion();
                nuevoCH.setFechaInicioReserva(Date.valueOf(vista.datePickerFechaIniCH.getDate()));
                nuevoCH.setFechaFinReserva(Date.valueOf(vista.datePickerFechaFinCH.getDate()));
                nuevoCH.setPrecioTotal(Double.parseDouble(vista.txtPrecioTotalCH.getText()));
                nuevoCH.setHabitacion((Habitacion) vista.dcbmHabitacionCH.getSelectedItem());
                nuevoCH.setCliente((Cliente) vista.dcbmClienteCH.getSelectedItem());
                modelo.altaClienteHabitacion(nuevoCH);

                break;

            case "ListarCH":
                listarClientesCH(modelo.getClienteHabitaciones());
                System.out.println("listaha");
                break;

            case "ModificarCH":
                ClienteHabitacion clienteHabitacionSeleccion = (ClienteHabitacion)vista.listClienteHabitacion.getSelectedValue();
                clienteHabitacionSeleccion.setFechaInicioReserva(Date.valueOf(vista.datePickerFechaIniCH.getDate()));
                clienteHabitacionSeleccion.setFechaFinReserva(Date.valueOf(vista.datePickerFechaFinCH.getDate()));
                clienteHabitacionSeleccion.setPrecioTotal(Double.parseDouble(vista.txtPrecioTotalCH.getText()));
                clienteHabitacionSeleccion.setHabitacion((Habitacion) vista.dcbmHabitacionCH.getSelectedItem());
                clienteHabitacionSeleccion.setCliente((Cliente) vista.dcbmClienteCH.getSelectedItem());

                //agenciaSeleccion.setPropietario((Propietario)vista.listPropietarios.getSelectedValue());
                //System.out.println((Propietario)vista.listPropietarios.getSelectedValue());
                modelo.modificarClienteHabitacion(clienteHabitacionSeleccion);
                break;

            case "EliminarCH":
                ClienteHabitacion clienteHabitacionBorrado  = (ClienteHabitacion)vista.listClienteHabitacion.getSelectedValue();
                modelo.borrarClienteHabitacion(clienteHabitacionBorrado);
                break;




            case "ListarPropietarios":
                //listarPropietarios(modelo.getPropietarios());
                break;


        }
        listarAgencias(modelo.getAgencias());
        listarClientes(modelo.getClientes());
        listarHabitaciones(modelo.getHabitaciones());
        listarPedidos(modelo.getPedidos());
        listarHabitacionAgencias(modelo.getHabitacionAgencias());
        listarClientesCH(modelo.getClienteHabitaciones());

    }

    /**
     * Configuración de los Listener de cada botón que tenemos, imprescinbile para que funciones nuestra aplicación.
     * @param listener ActionListener
     */


    private void addActionListeners(ActionListener listener){
        vista.conexionItem.addActionListener(listener);
        vista.salirItem.addActionListener(listener);
        vista.btnAltaAgencia.addActionListener(listener);
        vista.btnModificarAgencia.addActionListener(listener);
        vista.btnBorrarAgencia.addActionListener(listener);
        vista.btnListarAgencias.addActionListener(listener);

        vista.btnAltaCliente.addActionListener(listener);
        vista.btnModificacionCliente.addActionListener(listener);
        vista.btnListarClientes.addActionListener(listener);
        vista.btnEliminarCliente.addActionListener(listener);

        vista.btnAltaHabitacion.addActionListener(listener);
        vista.btnModificarHabitacion.addActionListener(listener);
        vista.btnListarHabitaciones.addActionListener(listener);
        vista.btnEliminarHabitacion.addActionListener(listener);



        vista.btnAltaPedido.addActionListener(listener);
        vista.btnListarPedidos.addActionListener(listener);
        vista.btnModificarPedido.addActionListener(listener);
        vista.btnEliminarPedido.addActionListener(listener);
        vista.btnListarClientesPedido.addActionListener(listener);



        vista.btnAltaHabitacionAgencia.addActionListener(listener);
        vista.btnModficarHabitacionAgencia.addActionListener(listener);
        vista.btnEliminarHabitacionAgencia.addActionListener(listener);
        vista.btnListarHabitacionAgencia.addActionListener(listener);



        vista.btnAltaClienteHabitacion.addActionListener(listener);
        vista.btnModificarClienteHabitacion.addActionListener(listener);
        vista.btnEliminarClienteHabitacion.addActionListener(listener);
        vista.btnListarClienteHabitacion.addActionListener(listener);




    }


    /**
     * Método que configurará los listeners de los JList y nos permitirá utilizar nuestros JList e interactuar con ellos para nuestra respectivas funciones.
     * @param listener ListSelectionListener
     */
    private void addListSelectionListener(ListSelectionListener listener){
        vista.listHabitacion.addListSelectionListener(listener);
        vista.listAgencia.addListSelectionListener(listener);
        vista.listCliente.addListSelectionListener(listener);
        vista.listPedido.addListSelectionListener(listener);
        vista.listClientesEnPedido.addListSelectionListener(listener);
    //    vista.listPedidosCliente.addListSelectionListener(listener);
        vista.listHabitacionAgencia.addListSelectionListener(listener);
        vista.listClienteHabitacion.addListSelectionListener(listener);
    }


    /**
     * Método que nos configura los listeners de nuestros ComboBox y nos permitirá utilizarlos.
     * @param controlador ItemListener
     */
    private void addItemListeners(ItemListener controlador){
        vista.comboAgenciaAH.addItemListener(controlador);
        vista.comboHabitacionAH.addItemListener(controlador);
        vista.comboClienteCH.addItemListener(controlador);
        vista.comboHabitacionCH.addItemListener(controlador);
        vista.comboClientePedido.addItemListener(controlador);
    }

    /**
     * Método que nos configura nuestros Listeners de foco.
     * @param controlador FocusListener
     */
    private void addFocusListeners(FocusListener controlador){
        vista.comboAgenciaAH.addFocusListener(controlador);
        vista.comboHabitacionAH.addFocusListener(controlador);
        vista.comboClienteCH.addFocusListener(controlador);
        vista.comboHabitacionCH.addFocusListener(controlador);
        vista.comboClientePedido.addFocusListener(controlador);
    }

    /**
     * Método que recibirá un arraylist de los datos de la clase Agencia y nos los añadir a nuestro DefaultListModel de Agencia.
     * @param lista ArrayList<Agencia>
     */
    public void listarAgencias(ArrayList<Agencia> lista){
        vista.dlmAgencia.clear();
        for(Agencia unaAgencia: lista){
            System.out.println(unaAgencia.getCorreo());
            vista.dlmAgencia.addElement(unaAgencia);
        }
    }

    /**
     * Método que recibirá un arraylist de los datos de la clase Cliente y nos los añadir a nuestro DefaultListModel de Cliente.
     * @param lista ArrayList<Cliente>
     */
    public void listarClientes(ArrayList<Cliente> lista){
        vista.dlmClientes.clear();
        for(Cliente unCliente: lista){
            System.out.println(unCliente.toString());
            vista.dlmClientes.addElement(unCliente);
        }
    }

    /**
     * Método que recibirá un arraylist de los datos de la clase Habitacion y nos los añadir a nuestro DefaultListModel de Cliente.
     * @param lista ArrayList<Habitacion>
     */
    public void listarHabitaciones(ArrayList<Habitacion> lista){
        vista.dlmHabitaciones.clear();
        for(Habitacion unaHabitacion: lista){
            System.out.println(unaHabitacion.toString());
            vista.dlmHabitaciones.addElement(unaHabitacion);
        }
    }

    /**
     * Método que recibirá un arraylist de los datos de la clase Pedido y nos los añadir a nuestro DefaultListModel de Pedido.
     * @param lista ArrayList<Pedido>
     */
    public void listarPedidos(ArrayList<Pedido> lista){
        vista.dlmPedidos.clear();
        for(Pedido unPedido: lista){
            System.out.println(unPedido.toString());
            vista.dlmPedidos.addElement(unPedido);
        }
    }

    /**
     * Método que recibirá un arraylist de los datos de la clase Cliente y nos los añadir a nuestro DefaultListModel de Clientes en la pestaña
     * de Pedido.
     * @param lista ArrayList<Cliente>
     */
    public void listarClientesEnPedidos(ArrayList<Cliente> lista){
        vista.dlmClientesEnPedido.clear();
        for(Cliente unCliente: lista){
            System.out.println(unCliente.toString());
            vista.dlmClientesEnPedido.addElement(unCliente);
        }
    }

    /**
     * Método que recibirá un arraylist de los datos de la clase HabitacionAgencia y nos los añadir a nuestro DefaultListModel de HabitacionAgencia.
     * @param lista ArrayList<HabitacionAgencia>
     */
    public void listarHabitacionAgencias(ArrayList<HabitacionAgencia> lista){
        vista.dlmHabitacionAgencias.clear();
        for(HabitacionAgencia unHabitacionAgencia: lista){
            System.out.println(unHabitacionAgencia.toString());
            vista.dlmHabitacionAgencias.addElement(unHabitacionAgencia);
        }
    }


    /**
     * Método que recibirá un arraylist de los datos de la clase ClienteHabitacion y nos los añadir a nuestro DefaultListModel de ClienteHabitacion.
     * @param lista ArrayList<ClienteHabitacion>
     */
    public void listarClientesCH(ArrayList<ClienteHabitacion> lista){
        vista.dlmClienteHabitacion.clear();
        for(ClienteHabitacion unClienteHabitacion: lista){
            System.out.println(unClienteHabitacion.toString());
            vista.dlmClienteHabitacion.addElement(unClienteHabitacion);
        }
    }



    @Override
    public void itemStateChanged(ItemEvent e) {
        if(e.getSource() == vista.comboAgenciaAH && e.getStateChange() == ItemEvent.SELECTED) {
           // listarAgenciasAH();

        }else if(e.getSource() == vista.comboHabitacionAH && e.getStateChange() == ItemEvent.SELECTED){
           // listarHabitacionesAH();
        }
    }

    /**
     * Método que se ejecuta cuando un comboBox gana el foco.
     * @param e FocusEvent
     */
    @Override
    public void focusGained(FocusEvent e) {
        if(e.getSource() == vista.comboHabitacionAH) {
            //Cuando pincho en el combo, no despliego el menu
            vista.comboHabitacionAH.hidePopup();
            listarHabitacionesAH();
            System.out.println("hola");
            //Despues de cargar los datos, si despliego el menu
            vista.comboHabitacionAH.showPopup();
        }else if(e.getSource() == vista.comboAgenciaAH){
            vista.comboAgenciaAH.hidePopup();
            listarAgenciasAH();
            vista.comboAgenciaAH.showPopup();
        }else if(e.getSource() == vista.comboClienteCH){
            vista.comboClienteCH.hidePopup();
            listarClientesCH();
            vista.comboClienteCH.showPopup();
        }else if(e.getSource() == vista.comboHabitacionCH){
            vista.comboHabitacionCH.hidePopup();
            listarHabitacionesCH();
            vista.comboHabitacionCH.showPopup();
        }else if(e.getSource() == vista.comboClientePedido){
            vista.comboClientePedido.hidePopup();
            listarClientesPedidoCombo();
            vista.comboClientePedido.showPopup();
        }
    }

    @Override
    public void focusLost(FocusEvent e) {

    }


    /**
     * Método que recibirá un arraylist de los datos de la clase Agencia y nos los añade a nuestro DefaultComboBoxModel de Agencia en la
     * pestaña AgenciaHabitación.
     *
     */
    private void listarAgenciasAH(){
        List<Agencia> listaAgencias = modelo.getAgencias();
        vista.dcbmAgenciaAH.removeAllElements();
        for(Agencia agencia : listaAgencias){
            vista.dcbmAgenciaAH.addElement(agencia);
        }
    }


    /**
     * Método que recibirá un arraylist de los datos de la clase Habitacion y nos los añade a nuestro DefaultComboBoxModel de Habitación en la
     * pestaña AgenciaHabitación.
     *
     */
    private void listarHabitacionesAH(){
        List<Habitacion> listaHabitaciones = modelo.getHabitaciones();
       // vista.dcbmHabitacionAH.removeAllElements();
        vista.dcbmHabitacionAH.removeAllElements();
        int i=0;
        for(Habitacion habitacion : listaHabitaciones){
            vista.dcbmHabitacionAH.addElement(habitacion);
            System.out.println(i++);
        }
    }


    /**
     * Método que recibirá un arraylist de los datos de la clase Habitacion y nos los añade a nuestro DefaultComboBoxModel de Habitacion en la
     * pestaña ClienteHabitación.
     *
     */
    private void listarHabitacionesCH(){
        List<Habitacion> listaHabitaciones = modelo.getHabitaciones();
        vista.dcbmHabitacionCH.removeAllElements();
        for(Habitacion habitacion : listaHabitaciones){
            vista.dcbmHabitacionCH.addElement(habitacion);
        }
    }

    /**
     * Método que recibirá un arraylist de los datos de la clase Cliente y nos los añade a nuestro DefaultComboBoxModel de Cliente en la
     * pestaña Cliente Habitación.
     *
     */
    private void listarClientesCH(){
        List<Cliente> listaClientes = modelo.getClientes();
        vista.dcbmClienteCH.removeAllElements();
        for(Cliente cliente : listaClientes){
            vista.dcbmClienteCH.addElement(cliente);
        }
    }

    /**
     * Método que recibirá un arraylist de los datos de la clase Cliente y nos los añade a nuestro DefaultComboBoxModel de Cliente en la
     * pestaña Pedido.
     *
     */
    private void listarClientesPedidoCombo(){
        List<Cliente> listaClientes = modelo.getClientes();
        vista.dcbmClientePedido.removeAllElements();
        for(Cliente cliente : listaClientes){
            vista.dcbmClientePedido.addElement(cliente);
        }
    }

    /**
     *
     * Método que nos detectará cuando pulsemos sobre un fila de un JList y nos rellenará los datos de cada JTextField
     * con sus correspondiente datos del objeto/dato seleccionado.
     * @param e ListSelectionEvent
     */

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){
            if(e.getSource() == vista.listAgencia) {
                Agencia agenciaSeleccion = (Agencia) vista.listAgencia.getSelectedValue();
                vista.txtIdAgencia.setText(String.valueOf(agenciaSeleccion.getId()));
                vista.txtNombreAgencia.setText(agenciaSeleccion.getNombre());
                vista.txtCodigoEmpresa.setText(agenciaSeleccion.getCodigoEmpresa());
                vista.dateFechaCreacionAgencia.setDate(agenciaSeleccion.getFechaCreacion().toLocalDate());
                vista.txtTelefonoAgencia.setText(String.valueOf(agenciaSeleccion.getTelefono()));
                vista.txtDireccionAgencia.setText(agenciaSeleccion.getDireccion());
                vista.txtCorreoElectronicoAgencia.setText(agenciaSeleccion.getCorreo());
            }
            if(e.getSource() == vista.listCliente) {
                Cliente clienteSeleccion = (Cliente) vista.listCliente.getSelectedValue();
                vista.txtIdCliente.setText(String.valueOf(clienteSeleccion.getId()));
                vista.txtNombreCliente.setText(clienteSeleccion.getNombre());
                vista.txtApellidosCliente.setText(clienteSeleccion.getApellidos());
                vista.datePickerFechaNCliente.setDate(clienteSeleccion.getFechaNacimiento().toLocalDate());
                vista.txtDomicilioCliente.setText(clienteSeleccion.getDomicilio());
                vista.txtDniCliente.setText(clienteSeleccion.getDni());
                vista.txtMovilCliente.setText(String.valueOf(clienteSeleccion.getNumeroMovil()));
                vista.txtTipoCliente.setText(clienteSeleccion.getTipoCliente());
            }
            if(e.getSource() == vista.listHabitacion) {
                Habitacion habitacionSeleccion = (Habitacion) vista.listHabitacion.getSelectedValue();
                vista.txtIdHabitacion.setText(String.valueOf(habitacionSeleccion.getId()));
                vista.txtNumeroHabitacion.setText(String.valueOf(habitacionSeleccion.getNumeroHabitacion()));
                vista.txtTipoHabitacion.setText(habitacionSeleccion.getTipo());
                vista.txtPrecioNocheHabitacion.setText(String.valueOf(habitacionSeleccion.getPrecioNoche()));
                vista.txtVistasHabitacion.setText(habitacionSeleccion.getVistas());
                vista.txtExtrasHabitacion.setText(habitacionSeleccion.getExtras());
            }
            if(e.getSource() == vista.listPedido) {
                Pedido pedidoSeleccion = (Pedido) vista.listPedido.getSelectedValue();
                vista.txtIdPedido.setText(String.valueOf(pedidoSeleccion.getId()));
                vista.txtPlatoComidaPedido.setText(pedidoSeleccion.getPlatoComida());
                vista.txtBebidaPedido.setText(String.valueOf(pedidoSeleccion.getBebida()));
                vista.datePickerPedido.setDate(pedidoSeleccion.getFechaConsumicion().toLocalDate());
                vista.txtPrecioTotal.setText(String.valueOf(pedidoSeleccion.getPrecioTotal()));
                vista.dcbmClientePedido.setSelectedItem(pedidoSeleccion.getCliente());
            }

            if(e.getSource() == vista.listHabitacionAgencia) {
                HabitacionAgencia habitacionAgenciaSeleccion = (HabitacionAgencia) vista.listHabitacionAgencia.getSelectedValue();
                vista.txtIdHA.setText(String.valueOf(habitacionAgenciaSeleccion.getId()));
                vista.datePickerFechaIniHA.setDate(habitacionAgenciaSeleccion.getFechaInicioReserva().toLocalDate());
                vista.datePickerFechaFinHA.setDate(habitacionAgenciaSeleccion.getFechaFinReserva().toLocalDate());
                vista.txtPrecioTotalHA.setText(String.valueOf(habitacionAgenciaSeleccion.getPrecioTotal()));
                vista.dcbmAgenciaAH.setSelectedItem(habitacionAgenciaSeleccion.getAgencia());
                vista.dcbmHabitacionAH.setSelectedItem(habitacionAgenciaSeleccion.getHabitacion());
            }
            if(e.getSource() == vista.listClienteHabitacion) {
                ClienteHabitacion clienteHabitacionSeleccion = (ClienteHabitacion) vista.listClienteHabitacion.getSelectedValue();
                vista.txtIdCH.setText(String.valueOf(clienteHabitacionSeleccion.getId()));
                vista.datePickerFechaIniCH.setDate(clienteHabitacionSeleccion.getFechaInicioReserva().toLocalDate());
                vista.datePickerFechaFinCH.setDate(clienteHabitacionSeleccion.getFechaFinReserva().toLocalDate());
                vista.txtPrecioTotalCH.setText(String.valueOf(clienteHabitacionSeleccion.getPrecioTotal()));
                vista.dcbmClienteCH.setSelectedItem(clienteHabitacionSeleccion.getCliente());
                vista.dcbmHabitacionCH.setSelectedItem(clienteHabitacionSeleccion.getHabitacion());
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void stateChanged(ChangeEvent e) {

    }
}
