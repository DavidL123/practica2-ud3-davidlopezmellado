/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.hibernate.gui;

import com.davidlm.hibernate.base.Agencia;
import com.davidlm.hibernate.base.Cliente;
import com.davidlm.hibernate.base.Habitacion;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;

/**
 * Clase Vista, para manejar todo lo que tiene que ver con la parte gráfica del programa.
 */
public class Vista extends Frame {
    private JPanel panel1;
    private JFrame frame;
     JTabbedPane tabbedPane1;
     JButton btnBorrarAgencia;
     JButton btnAltaAgencia;
     JButton btnModificarAgencia;
     JTextField txtNombreAgencia;
     JTextField txtCodigoEmpresa;
     JTextField txtTelefonoAgencia;
     DatePicker dateFechaCreacionAgencia;
     JTextField txtDireccionAgencia;
     JTextField txtCorreoElectronicoAgencia;
     JList listAgencia;
    JTextField txtDomicilioCliente;
     JTextField txtDniCliente;
    JTextField txtIdAgencia;
     JTextField txtIdHabitacion;
     JTextField txtNumeroHabitacion;
     JTextField txtTipoHabitacion;
     JTextField txtPrecioNocheHabitacion;
     JTextField txtVistasHabitacion;
     JTextField txtExtrasHabitacion;
     JButton btnAltaHabitacion;
     JButton btnModificarHabitacion;
     JButton btnEliminarHabitacion;
     JButton btnAltaCliente;
     JButton btnModificacionCliente;
     JButton btnEliminarCliente;
     JList listCliente;
     JList listHabitacion;
     JTextField txtMovilCliente;
     JTextField txtTipoCliente;
     JTextField txtNombreCliente;
     JTextField txtApellidosCliente;
     DatePicker datePickerFechaNCliente;
     JButton btnListarAgencias;
     JButton btnListarClientes;
     JButton btnListarHabitaciones;
     JTextField txtIdCliente;
     JTextField txtIdPedido;
     JTextField txtPlatoComidaPedido;
     JTextField txtBebidaPedido;
     JTextField txtPrecioTotal;
     DatePicker datePickerPedido;
    JButton btnAltaPedido;
     JButton btnListarPedidos;
     JButton btnModificarPedido;
     JButton btnEliminarPedido;
     JList listPedido;
     JButton btnListarClientesPedido;
    // JList listPedidosCliente;
     JList listClientesEnPedido;




     JTextField txtIdHA;
     JTextField txtPrecioTotalHA;
     JTextField txtIdCH;
     JTextField txtPrecioTotalCH;
     JComboBox comboHabitacionCH;
     JComboBox comboClienteCH;
    JComboBox comboAgenciaAH;
     JButton btnAltaClienteHabitacion;
     JButton btnListarClienteHabitacion;
     JButton btnModificarClienteHabitacion;
     JButton btnEliminarClienteHabitacion;
     JList listClienteHabitacion;
     JComboBox comboHabitacionAH;
     JButton btnAltaHabitacionAgencia;
     JButton btnListarHabitacionAgencia;
     JButton btnModficarHabitacionAgencia;
     JButton btnEliminarHabitacionAgencia;
     JList listHabitacionAgencia;
     DatePicker datePickerFechaIniCH;
     DatePicker datePickerFechaFinCH;
     DatePicker datePickerFechaIniHA;
     DatePicker datePickerFechaFinHA;
     JComboBox comboClientePedido;
    //JScrollPane jlistReserva;

    DefaultListModel dlmAgencia;
    DefaultListModel dlmClientes;
    DefaultListModel dlmHabitaciones;
    DefaultListModel dlmPedidos;
    DefaultListModel dlmClientesEnPedido;
    DefaultListModel dlmHabitacionAgencias;
    DefaultListModel dlmClienteHabitacion;



    DefaultComboBoxModel<Agencia> dcbmAgenciaAH;
    DefaultComboBoxModel<Habitacion> dcbmHabitacionAH;
    DefaultComboBoxModel<Cliente> dcbmClienteCH;
    DefaultComboBoxModel<Habitacion> dcbmHabitacionCH;
    DefaultComboBoxModel<Cliente> dcbmClientePedido;


    JMenuItem conexionItem;
    JMenuItem salirItem;


    /**
     * Constructor de la clase Vista. Crea el JFrame, indica el tamaño y carga varios métodos.
     */
    public Vista(){
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.setSize(new Dimension(this.getWidth()+50, this.getHeight()+50));
        frame.setLocationRelativeTo(null);

        crearMenu();
        crearModelos();
        inicializarCombos();
        frame.pack();
        frame.setVisible(true);

    }

    /**
     * Método que nos crea nuestros DefaultListModel
     */
    private void crearModelos() {
        dlmAgencia = new DefaultListModel();
        listAgencia.setModel(dlmAgencia);
        dlmClientes = new DefaultListModel();
        listCliente.setModel(dlmClientes);
        dlmHabitaciones = new DefaultListModel();
        listHabitacion.setModel(dlmHabitaciones);
        dlmPedidos=new DefaultListModel();
        listPedido.setModel(dlmPedidos);
        dlmClientesEnPedido=new DefaultListModel();
        listClientesEnPedido.setModel(dlmClientesEnPedido);
        dlmHabitacionAgencias= new DefaultListModel();
        listHabitacionAgencia.setModel(dlmHabitacionAgencias);
        dlmClienteHabitacion=new DefaultListModel();
        listClienteHabitacion.setModel(dlmClienteHabitacion);

    }

    /**
     * Método que nos crea la barra de menú y los distintos botones que tiene.
     */
    private void crearMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");

        conexionItem = new JMenuItem("Conectar");
        conexionItem.setActionCommand("Conectar");

        salirItem = new JMenuItem("Salir");
        salirItem.setActionCommand("Salir");

        menu.add(conexionItem);
        menu.add(salirItem);
        barra.add(menu);
        frame.setJMenuBar(barra);
    }

    /**
     * Método que nos crea nuestros DefaultComboBoxModel
     */
    private void inicializarCombos(){
        dcbmAgenciaAH = new DefaultComboBoxModel<>();
        comboAgenciaAH.setModel(dcbmAgenciaAH);
        dcbmHabitacionAH =  new DefaultComboBoxModel<>();
        comboHabitacionAH.setModel(dcbmHabitacionAH);
        dcbmClienteCH = new DefaultComboBoxModel<>();
        comboClienteCH.setModel(dcbmClienteCH);
        dcbmHabitacionCH = new DefaultComboBoxModel<>();
        comboHabitacionCH.setModel(dcbmHabitacionCH);
        dcbmClientePedido=new DefaultComboBoxModel<>();
        comboClientePedido.setModel(dcbmClientePedido);

    }

}
