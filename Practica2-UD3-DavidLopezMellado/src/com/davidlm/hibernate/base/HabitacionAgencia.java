/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.hibernate.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;
/**
 * Clase HabitacionAgencia. Con sus correspondientes campos.
 */
@Entity
@Table(name = "habitacion_agencia", schema = "basehotel", catalog = "")
public class HabitacionAgencia {
    private int id;
    private Date fechaInicioReserva;
    private Date fechaFinReserva;
    private double precioTotal;
    private Agencia agencia;
    private Habitacion habitacion;

    /**
     * Indica que es el campo id de la tabla, también el nombre de la columna, el método para obtener el dato y el método para modificarlo.
     * @return Devuelve el id
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Indica que es un atributo de la tabla(fecha_inicio_reserva), también el nombre de la columna, el método para obtener el dato y modificarlo.
     *
     * @return Devuelve la fecha de inicio de reserva
     */
    @Basic
    @Column(name = "fecha_inicio_reserva")
    public Date getFechaInicioReserva() {
        return fechaInicioReserva;
    }

    public void setFechaInicioReserva(Date fechaInicioReserva) {
        this.fechaInicioReserva = fechaInicioReserva;
    }

    /**
     * Indica que es un atributo de la tabla(fecha_fin_reserva), también el nombre de la columna, el método para obtener el dato y modificarlo.
     *
     * @return Devuelve la fecha de fin de reserva
     */
    @Basic
    @Column(name = "fecha_fin_reserva")
    public Date getFechaFinReserva() {
        return fechaFinReserva;
    }

    public void setFechaFinReserva(Date fechaFinReserva) {
        this.fechaFinReserva = fechaFinReserva;
    }

    /**
     * Indica que es un atributo de la tabla(precio_total), también el nombre de la columna, el método para obtener el dato y modificarlo.
     *
     * @return Devuelve el precio total
     */
    @Basic
    @Column(name = "precio_total")
    public double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(double precioTotal) {
        this.precioTotal = precioTotal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HabitacionAgencia that = (HabitacionAgencia) o;
        return id == that.id &&
                Double.compare(that.precioTotal, precioTotal) == 0 &&
                Objects.equals(fechaInicioReserva, that.fechaInicioReserva) &&
                Objects.equals(fechaFinReserva, that.fechaFinReserva);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fechaInicioReserva, fechaFinReserva, precioTotal);
    }


    /**
     * Relación de muchos a uno. Un reserva de agencia solo puede tener una agencia.
     * @return Devuelve el id de agencia
     */
    @ManyToOne
    @JoinColumn(name = "idagencia", referencedColumnName = "id", nullable = false)
    public Agencia getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencia agencia) {
        this.agencia = agencia;
    }

    /**
     * Relación de muchos a uno. Un reserva de agencia solo puede tener una habitacion.
     * @return Devuelve el id de habitacion
     */
    @ManyToOne
    @JoinColumn(name = "idhabitacion", referencedColumnName = "id", nullable = false)
    public Habitacion getHabitacion() {
        return habitacion;
    }

    public void setHabitacion(Habitacion habitacion) {
        this.habitacion = habitacion;
    }


    /**
     * Método toString para la clase HabitacionAgencia
     * @return Devuelve el toString
     */
    @Override
    public String toString() {
        return fechaInicioReserva+"-"+fechaFinReserva+"-"+precioTotal+"-Agencia: "+agencia+"-Habitacion: "+habitacion;
    }
}
