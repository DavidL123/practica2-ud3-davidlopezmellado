/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.hibernate.base;

import com.davidlm.hibernate.gui.Controlador;
import com.davidlm.hibernate.gui.Modelo;
import com.davidlm.hibernate.gui.Vista;

public class Principal {
    /**
     * Método main que crea objetos de clase Vista, Modelo y Controlador. En el objeto controlador al crearlo, le cargamos en el constructor
     * los objetos vista y modelo.
     * @param args
     */
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista,modelo);
    }
}
